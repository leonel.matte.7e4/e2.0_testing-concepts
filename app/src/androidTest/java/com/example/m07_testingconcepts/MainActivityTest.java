package com.example.m07_testingconcepts;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    //Activity 2 - Simple Activity App Testing

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule
            = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void elements_displayed_correctly() {

        onView(withId(R.id.activity_main_title)).check(matches(isDisplayed()));
        onView(withId(R.id.activity_main_next_button)).check(matches(isDisplayed()));

    }

    @Test
    public void check_string_values_displayed(){
        onView(withId(R.id.activity_main_title)).check(matches(withText("Main Activity Title!")));
        onView(withId(R.id.activity_main_next_button)).check(matches(withText("Next")));
    }

    @Test
    public void check_if_button_changes_text_after_click() {
        onView(withId(R.id.activity_main_next_button)).perform(click()).check(matches(withText("Back")));
    }
}
