package com.example.m07_testingconcepts;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class SecondActivityTest {

    //Activity 4 - Testing an app with multiple activities

    @Rule
    public ActivityScenarioRule<FirstActivity> activityScenarioRule
            = new ActivityScenarioRule<>(FirstActivity.class);

    @Test
    public void button_next_clicked() {

        onView(withId(R.id.activity_main2_button_next)).perform(click());
        onView(withId(R.id.activity_welcome_title)).check(matches(isDisplayed()));
    }

    @Test
    public void button_next_clicked_and_back_button_clicked() {

        onView(withId(R.id.activity_main2_button_next)).perform(click());
        onView(withId(R.id.activity_welcome_title)).check(matches(isDisplayed()));

        onView(withId(R.id.activity_welcome_button_back)).perform(click());
        onView(withId(R.id.activity_main2_title)).check(matches(isDisplayed()));

    }


}
