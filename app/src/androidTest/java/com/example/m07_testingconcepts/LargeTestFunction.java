package com.example.m07_testingconcepts;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class LargeTestFunction {

    String USERNAME_SAMPLE = "Lion";
    String TITLE_WELCOME_ACTIVITY = USERNAME_SAMPLE;
    String PASSWORD_SAMPLE = "1234";

    @Rule
    public ActivityScenarioRule<FirstActivity> activityScenarioRule
            = new ActivityScenarioRule<>(FirstActivity.class);

    @Test
    public void full_functionality_tet() {

        onView(withId(R.id.activity_main2_userEdit)).perform(typeText(USERNAME_SAMPLE)).perform(closeSoftKeyboard());
        onView(withId(R.id.activity_main2_passEdit)).perform(typeText(PASSWORD_SAMPLE)).perform(closeSoftKeyboard());
        onView(withId(R.id.activity_main2_button_next)).perform(click());
        onView(withId(R.id.activity_main2_title)).check(matches(isDisplayed()));
        onView(withId(R.id.activity_welcome_title)).check(matches(withText(TITLE_WELCOME_ACTIVITY)));
        onView(withId(R.id.activity_welcome_button_back)).perform(click());
        onView(withId(R.id.activity_main2_title)).check(matches(isDisplayed()));
        onView(withId(R.id.activity_main2_userEdit)).check(matches(withText("")));
        onView(withId(R.id.activity_main2_passEdit)).check(matches(withText("")));

    }
}
