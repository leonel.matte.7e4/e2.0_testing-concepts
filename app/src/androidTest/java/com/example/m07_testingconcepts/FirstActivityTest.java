package com.example.m07_testingconcepts;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class FirstActivityTest {

    //Activity 3 - Testing other kind of views

    String USER_TO_BE_TYPED = "user";
    String PASS_TO_BE_TYPED = "1234";

    @Rule
    public ActivityScenarioRule<FirstActivity> activityScenarioRule
            = new ActivityScenarioRule<>(FirstActivity.class);


    @Test
    public void login_form_behaviour(){

        onView(withId(R.id.activity_main2_userEdit)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.activity_main2_passEdit)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.activity_main2_button_next)).perform(click()).check(matches(withText("Logged")));

    }
}
