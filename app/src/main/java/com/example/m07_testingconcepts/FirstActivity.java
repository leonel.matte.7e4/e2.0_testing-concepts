package com.example.m07_testingconcepts;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class FirstActivity extends AppCompatActivity {

    MaterialButton button_next, button_back;
    TextView title;
    TextInputEditText username_input, password_input;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        title = findViewById(R.id.activity_main2_title);

        username_input = findViewById(R.id.activity_main2_userEdit);
        password_input = findViewById(R.id.activity_main2_passEdit);

        button_next = findViewById(R.id.activity_main2_button_next);

        button_back = findViewById(R.id.activity_welcome_button_back);

        button_next.setOnClickListener(v -> {

            Intent intent = new Intent(FirstActivity.this, SecondActivity.class);

            String username_to_pass = username_input.getText().toString();

            intent.putExtra("username", username_to_pass);

            startActivity(intent);

        });
    }
}
