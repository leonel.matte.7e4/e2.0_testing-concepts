package com.example.m07_testingconcepts;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity {

    MaterialButton button_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button_next = findViewById(R.id.activity_main_next_button);

        button_next.setOnClickListener(v -> {
            button_next.setText("Back");
        });
    }
}