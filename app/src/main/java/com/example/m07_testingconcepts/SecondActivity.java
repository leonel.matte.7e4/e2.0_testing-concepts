package com.example.m07_testingconcepts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class SecondActivity extends AppCompatActivity {

    MaterialButton button_back;

    TextView welcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        button_back = findViewById(R.id.activity_welcome_button_back);

        Intent info = getIntent();
        String username = info.getStringExtra("username");


        welcome.setText(username);

        button_back.setOnClickListener(v -> {
            Intent intent = new Intent(SecondActivity.this, FirstActivity.class);
            startActivity(intent);
        });

    }
}